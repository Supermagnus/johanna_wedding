import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './main/components/home/home.component';
import { AboutUsComponent } from './main/components/about-us/about-us.component';
import { AboutLocationComponent } from './main/components/about-location/about-location.component';
import { AboutWeddingComponent } from './main/components/about-wedding/about-wedding.component';
import { AccommodationComponent } from './main/components/accommodation/accommodation.component';
import { WeddingNotificationComponent } from './main/components/wedding-notification/wedding-notification.component';
import { UploadComponent } from './main/components/upload/upload.component';
import { MainModule } from './main/main.module';
import { SlideshowComponent } from './main/components/slideshow/slideshow.component';
import { BigScreenModule } from 'angular-bigscreen';
import { MatSnackBarModule } from '@angular/material';

const routes: Routes = [
  {
    path: 'home',
    component: HomeComponent,
    data: {animation: 'Home'}
  },
  {
    path: 'happycouple',
    component: AboutUsComponent,
    data: {animation: 'AboutUs'}
  }, {
    path: 'location',
    component: AboutLocationComponent,
    data: {animation: 'Location'}
  },
  {
    path: 'wedding',
    component: AboutWeddingComponent,
    data: {animation: 'AboutWedding'}
  },
  {
    path: 'accommodation',
    component: AccommodationComponent,
    data: {animation: 'AboutWedding'}
  },
  {
    path: 'notification',
    component: WeddingNotificationComponent,
    data: {animation: 'WeddingNotification'}
  },
  {
    path: 'upload',
    component: UploadComponent,
    data: {animation: 'Upload'}
  },
  {
    path: 'slideshow',
    component: SlideshowComponent,
    data: {animation: 'Slideshow'}
  },
  {
    path: '',
    component: HomeComponent,
    data: {animation: 'Home'}
  }
];


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    FlexLayoutModule,
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    MainModule,
    BigScreenModule,
    MatSnackBarModule,
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent],
  exports: [
    CoreModule
  ]
})
export class AppModule { }
