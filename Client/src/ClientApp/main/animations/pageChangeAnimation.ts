import {
  transition,
  trigger,
  query,
  style,
  animate,
  group,
  animateChild,
  keyframes
} from '@angular/animations';

export const pageChangeAnimation =
  trigger('routeAnimations', [
    transition('* <=> *', [
      group([
        query(
          ':enter',
          [
            style({
              opacity: 1,
              // transform: 'translateY(9rem) rotate(-10deg)'
            }),
            // animate(
            //   '0.1s ease', keyframes([
            //     style({ opacity: 0, offset: 0 }),
            //     style({ opacity: 0.5, offset: 0.5 }),
            //     style({ opacity: 1,  offset: 1 }),
            //   ])
            // ),
            animateChild()
          ],
          { optional: true }
        ),
        query(
          ':leave',
          [
            style({
              opacity: 0,
              // transform: 'translateY(9recrm) rotate(-10deg)'
            })
            ],
          { optional: true }
        )
      ])
    ])
  ]);
