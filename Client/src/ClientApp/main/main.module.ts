
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { RouterModule } from '@angular/router';
import { DropzoneConfigInterface, DropzoneModule, DROPZONE_CONFIG } from 'ngx-dropzone-wrapper';
import { AboutLocationComponent } from './components/about-location/about-location.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { AboutWeddingComponent } from './components/about-wedding/about-wedding.component';
import { AccommodationComponent } from './components/accommodation/accommodation.component';
import { HeaderComponent } from './components/header/header.component';
import { HomeComponent } from './components/home/home.component';
import { SlideshowComponent } from './components/slideshow/slideshow.component';
import { UploadComponent } from './components/upload/upload.component';
import { WeddingNotificationComponent } from './components/wedding-notification/wedding-notification.component';
import { FormsModule } from '@angular/forms';

const DEFAULT_DROPZONE_CONFIG: DropzoneConfigInterface = {
  // Change this to your upload POST address:
   url: 'https://httpbin.org/post',
   maxFilesize: 15000,
   acceptedFiles: 'image/*'
 };

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    RouterModule,
    MatInputModule,
    FormsModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    DropzoneModule,
    MatMenuModule,
    MatFormFieldModule,
    ],
  declarations: [
    HeaderComponent,
    HomeComponent,
    AboutUsComponent,
    AboutWeddingComponent,
    AboutLocationComponent,
    UploadComponent,
    WeddingNotificationComponent,
    AccommodationComponent,
    SlideshowComponent
  ],
  exports: [
    HeaderComponent,
    HomeComponent,
    AboutUsComponent,
    AboutWeddingComponent,
    AboutLocationComponent,
    UploadComponent,
    WeddingNotificationComponent,
    AccommodationComponent,
    SlideshowComponent
  ],
  providers: [
    {
      provide: DROPZONE_CONFIG,
      useValue: DEFAULT_DROPZONE_CONFIG
    }
  ]
})
export class MainModule { }
