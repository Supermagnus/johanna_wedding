import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'app-accommodation',
  templateUrl: './accommodation.component.html',
  styleUrls: ['accommodation.component.scss']
})
export class AccommodationComponent {
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.position') position = 'relative';
}
