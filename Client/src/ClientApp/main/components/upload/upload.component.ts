import { Component, HostBinding } from '@angular/core';
import { DropzoneConfigInterface } from 'ngx-dropzone-wrapper';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['upload.component.scss']
})
export class UploadComponent {
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.position') position = 'relative';


  public config: DropzoneConfigInterface = {
    url: environment.uploadUrl,
    clickable: true,
    maxFiles: 10,
    autoReset: null,
    errorReset: null,
    cancelReset: null
  };



  public onUploadInit(args: any): void {
    console.log('onUploadInit:', args);
  }

  public onUploadError(args: any): void {
    console.log('onUploadError:', args);
  }

  public onUploadSuccess(args: any): void {
    console.log('onUploadSuccess:', args);
  }
}
