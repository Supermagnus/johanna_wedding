import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'app-about-wedding',
  templateUrl: './about-wedding.component.html',
  styleUrls: ['about-wedding.component.scss']
})
export class AboutWeddingComponent {
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.position') position = 'relative';

  constructor() {


  }
}
