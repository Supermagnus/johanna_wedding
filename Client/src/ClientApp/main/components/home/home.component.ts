import { Component, HostBinding } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['home.component.scss']
})
export class HomeComponent  {
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.position') position = 'relative';


}
