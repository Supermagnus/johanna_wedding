import { Component, ElementRef, HostBinding } from '@angular/core';
import { BigScreenService } from 'angular-bigscreen';
import { environment } from 'src/environments/environment.prod';

@Component({
  selector: 'app-slideshow',
  templateUrl: './slideshow.component.html',
  styleUrls: ['slideshow.component.scss']
})
export class SlideshowComponent {
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.position') position = 'relative';
  hideButton = false;
  isPlaying = true;
  private timeStamp: number;
  rotation = 0;
  slideshowContentUrl: string = environment.slideshowUrl + '/current/content';


  public getSlideshowImage(): string {
    if (!this.isPlaying) {
      return this.slideshowContentUrl;
    }
    if (this.timeStamp) {
      return this.slideshowContentUrl + '?' + this.timeStamp;
    }
    return this.slideshowContentUrl;
  }
  constructor(private bigScreenService: BigScreenService) {
    setInterval(() => this.timeStamp = (new Date()).getTime(), 5000);
    bigScreenService.onChange(() => this.hideButton = !this.hideButton);
  }

  fullscreen(): void {
    const parentDiv = document.getElementById('the-frame');
    const theFrame = new ElementRef(parentDiv);
    this.bigScreenService.request(theFrame.nativeElement);

  }
  rotate(degrees: number): void {
    this.rotation += degrees;
  }

  togglePause(): void {
    this.isPlaying = !this.isPlaying;
  }

}
