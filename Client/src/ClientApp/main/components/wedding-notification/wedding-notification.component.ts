import { Component, HostBinding } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-wedding-notification',
  templateUrl: './wedding-notification.component.html',
  styleUrls: ['wedding-notification.component.scss']
})
export class WeddingNotificationComponent {
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.position') position = 'relative';

  names = '';
  preferences = '';
  favourites = '';
  formSent = false;
  mailResponse = '';

  constructor(private http: HttpClient, private snackbar: MatSnackBar) {

  }

  sendMail(): void {

    const body = '<h3>' + this.names + '</h3>' +
      '<h4>Specialare</h4><p>' + this.preferences + '</p>' +
      '<h4>Favoriter</h4><p>' + this.favourites + '</p>';
    const requestOptions: Object = {
      responseType: 'text'
    };
    this.http.post<any>(environment.sendMailUrl, {
      subject: 'Anmälan: ' + this.names,
      body: body
    },
      requestOptions
    ).subscribe(res => {
      this.snackbar.open('Kul att du ska komma! Du är anmäld.', 'Ok', { duration: 10000 });
      this.formSent = true;
    },
      err => {
        console.log(err);
        this.snackbar.open('Något gick fel :( Skicka gärna ett mail till supermagnus@gmail.com ' +
          'med felmeddelandet: ' + err.message, 'Ok');
          this.formSent = true;
      });
  }
}
