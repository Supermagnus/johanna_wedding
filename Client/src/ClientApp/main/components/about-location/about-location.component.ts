import { Component, HostBinding } from '@angular/core';

@Component({
  selector: 'app-about-location',
  templateUrl: './about-location.component.html',
  styleUrls: ['about-location.component.scss']
})
export class AboutLocationComponent {
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.position') position = 'relative';
//https://www.pexels.com/photo/selective-focus-photography-of-wedding-wooden-sign-1711275/
  constructor() {


  }
}
