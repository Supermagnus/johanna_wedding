export class ExperienceItem {
  constructor(
    public id: number,
    public title: string,
    public period: string,
    public company: string,
    public skills: string[],
    public description: string

  ) {  }

}
