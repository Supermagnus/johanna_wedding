export class TermFacet {
  constructor(
    public displayName: string,
    public hits: TermFacetHit[]
  ) {  }

}

export class TermFacetHit {
  constructor(
    public value: string,
    public count: number,
    public isSelected: boolean
  ) {}
}
