import { ExperienceItem } from './experience';
import { TermFacet } from './termFacet';

export class ExperienceResult {
  constructor(
    public experiences: Array<ExperienceItem>,
    public termFacets: Array<TermFacet>


  ) {  }

}
