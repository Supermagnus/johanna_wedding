import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { pageChangeAnimation } from './main/animations/pageChangeAnimation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['app.component.scss'],
  animations: [pageChangeAnimation]
})
export class AppComponent {
  title = 'Johanna & Pontus == Sant';

  prepareRoute(outlet: RouterOutlet): string {
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
}
