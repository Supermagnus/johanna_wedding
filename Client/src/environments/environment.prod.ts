export const environment = {
  production: true,
  slideshowUrl: 'http://johannaochpontus.se/api/download/slideshow',
  uploadUrl: 'http://johannaochpontus.se/api/Upload',
  // slideshowUrl: 'http://localhost:5000/api/download/slideshow',
  // uploadUrl: 'http://localhost:5000/api/Upload',
  sendMailUrl:  'http://johannaochpontus.se/api/Mail',
  showSlideshow: true
};
