﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Serilog;
using Swashbuckle.AspNetCore.Swagger;
using Wedding.WebAPI.Domain.Settings;
using Wedding.WebAPI.Services;
using Wedding.WebAPI.Services.Mail;

namespace Wedding.WebAPI
{
    public class Startup
    {
        private IHostingEnvironment _currentEnv;
        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
            Configuration = configuration;
            _currentEnv = env;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.Console()
                .WriteTo.File("logs/log.txt", rollingInterval: RollingInterval.Day)
                .CreateLogger();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Wedding API", Version = "v1" });
            });

            services.AddSingleton<IImageService, FileSystemImageService>();
            services.AddSingleton<IMailService, GMailService>();
            var mailService = Configuration.GetSection("GMailService").Get<GMailServiceSettings>();
            var fileSystemImageServiceSettings = Configuration.GetSection("ImageService").Get<FileSystemImageServiceSettings>();
            services.AddSingleton(mailService);
            services.AddSingleton(fileSystemImageServiceSettings);

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Wedding API V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseCors(x => x

                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());


            app.UseMvc();
        }
    }
}
