﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Wedding.WebAPI.Domain.Settings
{
    public class FileSystemImageServiceSettings
    {
        public string RootDirectory { get; set; }
        public bool SlideshowRandomImage { get; set; } = true;
        public int SlideshowFrequencySec { get; set; } = 5;
    }
}
