﻿namespace Wedding.WebAPI.Domain.Settings
{
    public class GMailServiceSettings
    {
        public string SendAddress { get;set; }
        public string ToAddress { get; set; }
        public string Password { get; set; }
    }
}