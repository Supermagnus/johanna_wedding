﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Wedding.WebAPI.Domain
{
    public class ImageInfo
    {
        public string Id { get; set; }
        public string ContentType { get; set; }
        public string Uploader { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
