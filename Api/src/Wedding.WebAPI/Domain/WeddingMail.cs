﻿namespace Wedding.WebAPI.Controllers
{
    public class WeddingMail
    {
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}