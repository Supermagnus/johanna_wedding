﻿using Microsoft.AspNetCore.Mvc;

namespace Wedding.WebAPI.Domain
{
    public class Image
    {
        public FileContentResult FileContentResult { get; set; }
        public ImageInfo ImageInfo { get; set; }
    }
}