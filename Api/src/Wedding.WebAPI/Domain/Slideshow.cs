﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Wedding.WebAPI.Domain
{
    public class Slideshow
    {
        public string Id { get; set; }
        public List<string> AvailableImageIds { get; set; }
        public List<string> NotShownImageIds { get; set; } 

        public Slideshow(List<string> imageIds)
        {
            AvailableImageIds = new List<string>(imageIds);
            NotShownImageIds = new List<string>(AvailableImageIds);
        }

        public string CurrentImageId { get; set; }

        public bool HasShownAll()
        {
            return NotShownImageIds.Count == 0;
        }

        public string GetNextInOrder()
        {
            lock (NotShownImageIds)
            {
                if (!AvailableImageIds.Any())
                {
                    return "";
                }
                if (NotShownImageIds.Count == 0)
                {
                    Reset();
                }
                var nextInLine = NotShownImageIds.First();
                NotShownImageIds.RemoveAt(0);
                CurrentImageId = nextInLine;
                return nextInLine;
            }
        }

        public string GetNextAtRandom()
        {
            lock (NotShownImageIds)
            {
                if (!AvailableImageIds.Any())
                {
                    return "";
                }
                if (NotShownImageIds.Count == 0)
                {
                    Reset();
                }
                var index = new Random().Next(NotShownImageIds.Count);
                var nextInLine = NotShownImageIds[index];
                NotShownImageIds.RemoveAt(index);
                CurrentImageId = nextInLine;
                return nextInLine;
            }
        }
        public void AddImageId(string id)
        {
            AvailableImageIds.Add(id);
        }

        public void Reset()
        {
            NotShownImageIds = new List<string>(AvailableImageIds);
            CurrentImageId = "";
        }

        public int Total()
        {
            return AvailableImageIds.Count;
        }
    }
}