﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Newtonsoft.Json;
using Serilog;
using Wedding.WebAPI.Domain;
using Wedding.WebAPI.Domain.Settings;

namespace Wedding.WebAPI.Services
{
    public class FileSystemImageService : IImageService
    {
        private readonly FileSystemImageServiceSettings _settings;
        private readonly string _infoDirectory;
        private readonly string _imageDirectory;
        private readonly FileExtensionContentTypeProvider _contentTypeProvider;
        private readonly Slideshow _slideshow;

        private readonly Timer _slideshowTicker;

        public FileSystemImageService(FileSystemImageServiceSettings settings)
        {
            _settings = settings;
            _contentTypeProvider = new FileExtensionContentTypeProvider();
            _infoDirectory = Path.Combine(settings.RootDirectory, "info");
            _imageDirectory = Path.Combine(settings.RootDirectory, "images");
            CreateDirectoryIfNotExist(settings.RootDirectory);
            CreateDirectoryIfNotExist(_imageDirectory);
            CreateDirectoryIfNotExist(_infoDirectory);
            Log.Debug("Images will be persisted at {_rootDirectory}", settings.RootDirectory);

            _slideshow = InitiateSlideshow();
            Log.Debug("Initiated slideshow with {Count} images", _slideshow.AvailableImageIds.Count);
            _slideshowTicker = new Timer(NextImage, null, TimeSpan.Zero, TimeSpan.FromSeconds(settings.SlideshowFrequencySec));
            Log.Debug(
                !settings.SlideshowRandomImage
                    ? "Starting slideshow with a new image in order of creation every {SlideshowFrequencySec}"
                    : "Starting slideshow with a new image at random every {SlideshowFrequencySec}",
                settings.SlideshowFrequencySec);
        }

        private void NextImage(Object state)
        {
            var total = _slideshow.Total();

            if (_slideshow.HasShownAll())
            {
                _slideshow.Reset();
                Log.Debug("All images has been show, resetting slideshow ({total} images)", total);
            }

            if (_settings.SlideshowRandomImage)
            {
                var id = _slideshow.GetNextAtRandom();
                Log.Debug("Randomly picked image id {id} ({Count} left)", id, _slideshow.NotShownImageIds.Count);
            }
            else
            {
                var id =_slideshow.GetNextInOrder();
                Log.Debug("In order of creation, picked image id {id} ({Count} left)", id, _slideshow.NotShownImageIds.Count);
            }
            
        }
        public async Task<ImageInfo> Save(IFormFile file)
        {
            return await SaveImage(file);
        }

        public async Task<ImageInfo[]> Save(IEnumerable<IFormFile> files)
        {
            return await Task.WhenAll(files.Select(SaveImage));
        }

        public async Task<Image> Get(string id)
        {
            var path = GetImageFullPath(id);
            var infoPath = GetImageInfoFullPath(id);
            var content = await GetContentByPath(path);
            var info = await GetInfoByPath(infoPath);
            return new Image()
            {
                FileContentResult = content,
                ImageInfo = info
            };
        }


        public async Task<Slideshow> GetSlideshow()
        {
            return _slideshow;
        }

        public Task<Image> GetCurrentSlideshowImage()
        {
            return string.IsNullOrEmpty(_slideshow.CurrentImageId) ? null : Get(_slideshow.CurrentImageId);
        }

        private async Task<FileContentResult> GetContentByPath(string path)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("No file found at " + path);
            }
            if (!_contentTypeProvider.TryGetContentType(path, out var contentType))
            {
                contentType = "n/a";
            }

            var bytes = await File.ReadAllBytesAsync(path);

            return new FileContentResult(bytes, contentType);
        }
        private async Task<ImageInfo> GetInfoByPath(string path)
        {
            if (!File.Exists(path))
            {
                throw new FileNotFoundException("No file found at " + path);
            }

            var content = await File.ReadAllTextAsync(path);
            var imageInfo = JsonConvert.DeserializeObject<ImageInfo>(content);
            return imageInfo;
        }
        private async Task<ImageInfo> SaveImage(IFormFile file)
        {
            if (file.Length <= 0)
            {
                throw new ArgumentNullException(file.FileName + " not found");
            }

            string id = Guid.NewGuid() + file.FileName;
            var path = GetImageFullPath(id);
            string contentType = "";
            if (!string.IsNullOrEmpty(file.ContentType) && !_contentTypeProvider.TryGetContentType(path, out contentType))
            {
                contentType = "n/a";
            }

            using (var fileStream = new FileStream(path, FileMode.Create))
            {
                await file.CopyToAsync(fileStream);
            }
            _slideshow.AddImageId(id);
            return await SaveImageInfo(id, contentType);
        }

        private async Task<ImageInfo> SaveImageInfo(string id, string contentType)
        {
            var path = GetImageInfoFullPath(id);
            var imageInfo = new ImageInfo()
            {
                ContentType = contentType,
                CreateDate = DateTime.UtcNow,
                Id = id,
                Uploader = "unknown"
            };
            await File.WriteAllTextAsync(path, JsonConvert.SerializeObject(imageInfo));
            return imageInfo;
        }

        private string GetImageFullPath(string id)
        {
            return Path.Combine(_imageDirectory, id);
        }

        private string GetImageInfoFullPath(string id)
        {
            return Path.Combine(_infoDirectory, id) + ".json";
        }

        private void CreateDirectoryIfNotExist(string path)
        {
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                Log.Debug("Created {path}", path);
            }
        }

        private Slideshow InitiateSlideshow()
        {
            return new Slideshow(Directory.GetFiles(_imageDirectory)
                .Select(Path.GetFileName).ToList());
        }

    }
}
