﻿using System.Threading.Tasks;

namespace Wedding.WebAPI.Services.Mail
{
    public interface IMailService
    {
        void SendMail( string title, string body);
    }
}