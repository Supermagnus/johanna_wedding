﻿using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Serilog;
using Wedding.WebAPI.Domain.Settings;

namespace Wedding.WebAPI.Services.Mail
{
    public class GMailService : IMailService
    {
        private readonly GMailServiceSettings _gMailServiceSettings;

        public GMailService(GMailServiceSettings gMailServiceSettings)
        {
            _gMailServiceSettings = gMailServiceSettings;
            Log.Debug("Initializing GmailService with sender {SendAddress} and password {Password}", gMailServiceSettings.SendAddress, gMailServiceSettings.Password);

        }

        public void SendMail( string title, string body)
        {
            using (var message = new MailMessage())
            {
                message.To.Add(new MailAddress(_gMailServiceSettings.ToAddress));
                message.From = new MailAddress(_gMailServiceSettings.SendAddress, "Bröllopssidan");
                message.Subject = title;
                message.Body = body;
                message.IsBodyHtml = true;

                using (var client = new SmtpClient("smtp.gmail.com"))
                {
                    client.Port = 587;
                    client.Credentials = new NetworkCredential(_gMailServiceSettings.SendAddress, _gMailServiceSettings.Password);
                    client.EnableSsl = true;
                     client.Send(message);
                }
            }
        }
    }
}