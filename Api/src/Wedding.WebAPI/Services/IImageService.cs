﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Wedding.WebAPI.Domain;

namespace Wedding.WebAPI.Services
{
    public interface IImageService
    {
        Task<ImageInfo> Save(IFormFile file);
        Task<ImageInfo[]> Save(IEnumerable<IFormFile> files);

        Task<Image> Get(string id);

        Task<Image> GetCurrentSlideshowImage();

        Task<Slideshow> GetSlideshow();
    }
}