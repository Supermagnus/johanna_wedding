﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Serilog.Core;
using Wedding.WebAPI.Domain;
using Wedding.WebAPI.Domain.Settings;
using Wedding.WebAPI.Services;
using Wedding.WebAPI.Services.Mail;

namespace Wedding.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MailController : ControllerBase
    {
        private readonly IMailService _mailService;

        public MailController(IMailService mailService)
        {
            _mailService = mailService;
        }

        [HttpPost]
        public  IActionResult Send([FromBody] WeddingMail mail)
        {
            Log.Information("Sending mail: {mail}",mail);
             _mailService.SendMail( mail.Subject, mail.Body);
            return Ok("Mail sent");
        }

    }
}
