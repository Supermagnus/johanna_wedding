﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Wedding.WebAPI.Services;

namespace Wedding.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DownloadController : ControllerBase
    {
        private readonly IImageService _imageService;
        public DownloadController( IImageService imageService)
        {
            _imageService = imageService;
        }

        [HttpGet("image/{id}")]
        public async Task<IActionResult> GetImage(string id)
        {
            try
            {
                var result = await _imageService.Get(id);
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception e)
            {
                Log.Error(e, "Error when trying to get Image {id}", id);
                return NotFound();
            }
            
        }

        [HttpGet("slideshow/current/content")]
        public async Task<IActionResult> GetCurrentImageContent()
        {
            var result = await _imageService.GetCurrentSlideshowImage();
            if (result == null)
            {
                return new EmptyResult();
            }
            return result.FileContentResult;
        }

        [HttpGet("slideshow/current/info")]
        public async Task<IActionResult> GetCurrentImageInfo()
        {
            var result = await _imageService.GetCurrentSlideshowImage();
            if (result == null)
            {
                return new EmptyResult();
            }
            return Ok(result.ImageInfo);
        }

        [HttpGet("slideshow/current")]
        public async Task<IActionResult> GetCurrentImage()
        {
            var result = await _imageService.GetCurrentSlideshowImage();
            if (result == null)
            {
                return new EmptyResult();
            }
            return Ok(result);
        }


        [HttpGet("slideshow/overview")]
        public async Task<IActionResult> GetSlideshow()
        {
            return Ok(await _imageService.GetSlideshow());
        }




    }
}
