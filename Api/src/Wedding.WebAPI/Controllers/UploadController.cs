﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Serilog;
using Wedding.WebAPI.Domain;
using Wedding.WebAPI.Domain.Settings;
using Wedding.WebAPI.Services;

namespace Wedding.WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UploadController : ControllerBase
    {
        private readonly IImageService _imageService;
        public UploadController(IImageService imageService)
        {
            _imageService = imageService;
        }

        [HttpPost]
        public async Task<ActionResult<ImageInfo>> UploadFile(IFormFile file)
        {
            try
            {
                var imageInfo = await _imageService.Save(file);
                Log.Debug("Persisted image: {Id}", imageInfo.Id);
                return Ok(imageInfo);
            }
            catch (Exception e)
            {
                Log.Error(e, "Error when persisting image: {FileName}", file.FileName);
                return ValidationProblem();
            }
        }
    }
}
